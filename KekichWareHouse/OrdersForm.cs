﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Форма для отображения списка заказов.
    /// </summary>
    public partial class OrdersForm : Form
    {
        /// <summary>
        /// Статус выделенного заказа.
        /// </summary>
        public static int currentStatus;
        /// <summary>
        /// Список текущих заказов.
        /// </summary>
        List<Order> currentOrders;
        /// <summary>
        /// Выводим ли мы список активных заказов.
        /// </summary>
        bool active = false;
        /// <summary>
        /// Конструктор для создания формы.
        /// </summary>
        /// <param name="active">Активные ли заказы.</param>
        public OrdersForm(bool active=false)
        {
            InitializeComponent();
            this.active = active;
        }
        /// <summary>
        /// Перерисовка списка заказов.
        /// </summary>
        private void RedrawListView()
        {
            listView.Items.Clear();
            // Добавляем каждый заказ.
            foreach (Order order in currentOrders)
            {
                ListViewItem newItem = new ListViewItem($"{order.id}", 0);
                newItem.Font = new Font(newItem.Font, FontStyle.Bold);
                // Подсказка при наведении на заказ.
                newItem.ToolTipText = "Нажмите на заказ, чтобы развернуть.";
                newItem.SubItems.Add($"{order.status & OrderStatus.Processed}");
                newItem.SubItems.Add($"{order.status & OrderStatus.Paid}");
                newItem.SubItems.Add($"{order.status & OrderStatus.Shipped}");
                newItem.SubItems.Add($"{order.status & OrderStatus.Executed}");
                listView.Items.Add(newItem);
            }
        }
        /// <summary>
        /// Загрузка формы. Задание начальных значений.
        /// </summary>
        private void OrdersForm_Load(object sender, EventArgs e)
        {
            listView.View = View.Details;
            listView.GridLines = true;
            if (MainForm.authorizationType == AuthorizationType.User)
            {
                // Все заказы данного пользователя.
                currentOrders = (from o in Order.orders where o.user.email == MainForm.currentUser.email select o).ToList();
            }
            else
            {
                if (active)
                {
                    Text = "Список активных заказов";
                    // Все активные заказы.
                    currentOrders = (from o in Order.orders where (int)(o.status & OrderStatus.Executed) != (int)OrderStatus.Executed select o).ToList();
                }
                else
                {
                    Text = "Список всех заказов (кликните для изменения статуса)";
                    // Вообще все заказы.
                    currentOrders = Order.orders;
                }
            }
            RedrawListView();
        }
        /// <summary>
        /// Метод обработки нажатия на элемент listView.
        /// </summary>
        private void ListView_ItemActivate(object sender, EventArgs e)
        {
            if (MainForm.authorizationType == AuthorizationType.User)
            {
                // Оплата заказа.
                if ((int)(currentOrders[listView.SelectedItems[0].Index].status & OrderStatus.Processed) == 1)
                {
                    if (MessageBox.Show("Вы уверены, что хотите оплатить заказ?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        // Так как статусы задаются степенями двоек, для них работает побитовое или.
                        currentOrders[listView.SelectedItems[0].Index].status |= OrderStatus.Paid;
                        RedrawListView();
                    }
                }
                else
                {
                    MessageBox.Show("Заказ еще не обработан.", "Ошибка!");
                }
            }
            else if (!active)
            {
                // Изменение статуса заказа.
                var statusEditForm = new StatusEditForm();
                statusEditForm.ShowDialog();
                currentOrders[listView.SelectedItems[0].Index].status = (OrderStatus)currentStatus;
                Order.orders[listView.SelectedItems[0].Index].status = (OrderStatus)currentStatus;
                RedrawListView();
            }
        }
    }
}
