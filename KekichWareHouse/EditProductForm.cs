﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Форма, необходимая для редактирования товара.
    /// </summary>
    public partial class EditProductForm : Form
    {
        /// <summary>
        /// Тип формы.
        /// </summary>
        ProductFormType type;
        /// <summary>
        /// Перечисление, определяющее тип формы.
        /// </summary>
        public enum ProductFormType
        {
            /// <summary>
            /// Добавление нового товара.
            /// </summary>
            Add,
            /// <summary>
            /// Редактирование товара.
            /// </summary>
            Edit
        }
        /// <summary>
        /// Создание новой формы переданного типа.
        /// </summary>
        /// <param name="type">Тип формы.</param>
        public EditProductForm(ProductFormType type)
        {
            this.type = type;
            InitializeComponent();
        }
        /// <summary>
        /// Проверка введённой информации на валидность.
        /// </summary>
        /// <param name="count">Количество оставшегося товара.</param>
        /// <param name="price">Цена товара.</param>
        /// <returns>Валидны ли введённые данные.</returns>
        private bool CheckData(out uint count, out double price)
        {
            price = 0;
            count = 0;
            if (Product.CheckSame(articleTextBox.Text))
            {
                MessageBox.Show("Товар с данным артикулом уже существует в системе.",
                    "Ошибка");
                return false;
            }
            if (!uint.TryParse(countTextBox.Text, out count))
            {
                MessageBox.Show("Значение поля \"количество\" должно быть целым неотрицательным числом!",
                    "Ошибка");
                return false;
            }
            if (!double.TryParse(priceTextBox.Text, out price) || price < 0)
            {
                MessageBox.Show("Значение поля \"цена\" должно быть неотрицательным числом!",
                    "Ошибка");
                return false;
            }
            return true;
        }
        /// <summary>
        /// Нажатие кнопки ОК.
        /// Добавление или изменение товара.
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            MainForm main = this.Owner as MainForm;
            if (CheckData(out uint count, out double price))
            {
                if (type == ProductFormType.Add)
                {
                    AddProduct(main, count, price);
                }
                else
                {
                    EditProduct(main, count, price);
                }
                this.Close();
            }
        }
        /// <summary>
        /// Изменение товара.
        /// </summary>
        /// <param name="main">Основная форма, из которой вызвана текущая.</param>
        /// <param name="count">Количество оставшегося товара.</param>
        /// <param name="price">Цена товара.</param>
        private void EditProduct(MainForm main, uint count, double price)
        {
            try
            {
                // Изменение элемента listView.
                main.listView.SelectedItems[0].SubItems[0].Text = nameTextBox.Text;
                main.listView.SelectedItems[0].SubItems[1].Text = articleTextBox.Text;
                main.listView.SelectedItems[0].SubItems[2].Text = $"{count}";
                main.listView.SelectedItems[0].SubItems[3].Text = $"{price}";
                // Изменение объекта товара, соответствующего данному элементу.
                var currentChapter = MainForm.findChapter[main.treeView.SelectedNode];
                var product = currentChapter.products[main.listView.SelectedIndices[0]];
                product.name = nameTextBox.Text;
                product.article = articleTextBox.Text;
                product.count = count;
                product.price = price;
            }
            catch (Exception ex) when (ex is NullReferenceException || ex is IndexOutOfRangeException)
            {
                MessageBox.Show("Вы не выбрали товар. Попробуйте ещё раз.", "Ошибка");
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Раздел не выбран. Попробуйте ещё раз.", "Ошибка");
            }
            catch (KeyNotFoundException)
            {
                MessageBox.Show("Выделенный раздел не найден в системе. Попробуйте ещё раз.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Добавление товара.
        /// </summary>
        /// <param name="main">Основная форма, из которой вызвана текущая.</param>
        /// <param name="count">Количество оставшегося товара.</param>
        /// <param name="price">Цена товара.</param>
        private void AddProduct(MainForm main, uint count, double price)
        {
            try
            {
                // Добавление товара в listView.
                ListViewItem newItem = new ListViewItem(nameTextBox.Text, 0);
                newItem.Font = new Font(newItem.Font, FontStyle.Bold);
                // Подсказка при наведении на товар.
                newItem.ToolTipText = "Собственный товар раздела.";
                newItem.SubItems.Add(articleTextBox.Text);
                newItem.SubItems.Add($"{count}");
                newItem.SubItems.Add($"{price}");
                main.listView.Items.Add(newItem);
                // Добавление товара в список соответствующего раздела.
                var currentChapter = MainForm.findChapter[main.treeView.SelectedNode];
                currentChapter.products.Add(new Product(newItem));
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("Программа не предназначена для параллельного запуска. Закройте другое окно.", "Ошибка");
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Раздел не выбран. Попробуйте ещё раз.", "Ошибка");
            }
            catch (KeyNotFoundException)
            {
                MessageBox.Show("Выделенный раздел не найден в системе. Попробуйте ещё раз.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Загрузка формы.
        /// Изменение текста в компонентах.
        /// </summary>
        private void EditGoodForm_Load(object sender, EventArgs e)
        {
            if (type == ProductFormType.Add)
            {
                Text = "Добавление товара";
            }
            else
            {
                Text = "Изменение товара";
                MainForm main = this.Owner as MainForm;
                nameTextBox.Text = main.listView.SelectedItems[0].SubItems[0].Text;
                articleTextBox.Text = main.listView.SelectedItems[0].SubItems[1].Text;
                countTextBox.Text = main.listView.SelectedItems[0].SubItems[2].Text;
                priceTextBox.Text = main.listView.SelectedItems[0].SubItems[3].Text;
            }
        }
        /// <summary>
        /// Нажатие кнопки Отмена.
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
