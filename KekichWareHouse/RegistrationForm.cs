﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Форма регистрации нового пользователя.
    /// </summary>
    public partial class RegistrationForm : Form
    {
        public RegistrationForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Обработчик нажатия кнопки "Зарегистрироваться".
        /// Добавляет нового пользователя в список пользователей.
        /// </summary>
        private void RegistrationButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Пробуем создать нового пользователя.
                User newUser = new User(nameBox.Text, phoneBox.Text, adressBox.Text, emailBox.Text, passwordBox.Text);
                // Добавление элемента ТОЛЬКО если пользователь создан без ошибок.
                Array.Resize(ref User.users, User.users.Count() + 1);
                User.users[User.users.Count() - 1] = newUser;
                Close();
            }
            catch (Exception ex)
            {
                // Различные типы исключений уже обработаны в конструкторе класса User.
                MessageBox.Show(ex.Message, "Ошибка!");
            }
        }
        /// <summary>
        /// Нажатие кнопки "Отмена".
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            // Закрытие формы.
            Close();
        }
    }
}
