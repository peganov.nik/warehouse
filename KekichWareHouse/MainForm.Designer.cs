﻿
namespace KekichWareHouse
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.fileStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chapterStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.addChapterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToRootToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editChapterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteChapterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productsStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.addProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToBasketToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unselectStripButton = new System.Windows.Forms.ToolStripButton();
            this.generatorToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.settingsStripButton = new System.Windows.Forms.ToolStripButton();
            this.clientsToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.orderToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.treeView = new System.Windows.Forms.TreeView();
            this.listView = new System.Windows.Forms.ListView();
            this.Название = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Артикул = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Количество = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Цена = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.orderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ordersListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activeOrdersListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileStripDropDownButton,
            this.chapterStripDropDownButton,
            this.productsStripDropDownButton,
            this.unselectStripButton,
            this.generatorToolStripButton,
            this.settingsStripButton,
            this.clientsToolStripButton,
            this.orderToolStripDropDownButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(863, 27);
            this.toolStrip.TabIndex = 0;
            this.toolStrip.Text = "Раздел";
            // 
            // fileStripDropDownButton
            // 
            this.fileStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fileStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.reportToolStripMenuItem});
            this.fileStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("fileStripDropDownButton.Image")));
            this.fileStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileStripDropDownButton.Name = "fileStripDropDownButton";
            this.fileStripDropDownButton.Size = new System.Drawing.Size(59, 24);
            this.fileStripDropDownButton.Text = "Файл";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.openToolStripMenuItem.Text = "Открыть";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.saveToolStripMenuItem.Text = "Сохранить";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.saveAsToolStripMenuItem.Text = "Сохранить как";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.SaveAsToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.reportToolStripMenuItem.Text = "Сохранить отчёт";
            this.reportToolStripMenuItem.Click += new System.EventHandler(this.ReportToolStripMenuItem_Click);
            // 
            // chapterStripDropDownButton
            // 
            this.chapterStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.chapterStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addChapterToolStripMenuItem,
            this.addToRootToolStripMenuItem,
            this.editChapterToolStripMenuItem,
            this.deleteChapterToolStripMenuItem});
            this.chapterStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("chapterStripDropDownButton.Image")));
            this.chapterStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.chapterStripDropDownButton.Name = "chapterStripDropDownButton";
            this.chapterStripDropDownButton.Size = new System.Drawing.Size(70, 24);
            this.chapterStripDropDownButton.Text = "Раздел";
            // 
            // addChapterToolStripMenuItem
            // 
            this.addChapterToolStripMenuItem.Name = "addChapterToolStripMenuItem";
            this.addChapterToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.addChapterToolStripMenuItem.Text = "Добавить";
            this.addChapterToolStripMenuItem.Click += new System.EventHandler(this.AddChapterToolStripMenuItem_Click);
            // 
            // addToRootToolStripMenuItem
            // 
            this.addToRootToolStripMenuItem.Name = "addToRootToolStripMenuItem";
            this.addToRootToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.addToRootToolStripMenuItem.Text = "Добавить в корень";
            this.addToRootToolStripMenuItem.Click += new System.EventHandler(this.AddToRootToolStripMenuItem_Click);
            // 
            // editChapterToolStripMenuItem
            // 
            this.editChapterToolStripMenuItem.Enabled = false;
            this.editChapterToolStripMenuItem.Name = "editChapterToolStripMenuItem";
            this.editChapterToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.editChapterToolStripMenuItem.Text = "Изменить";
            this.editChapterToolStripMenuItem.Click += new System.EventHandler(this.EditChapterToolStripMenuItem);
            // 
            // deleteChapterToolStripMenuItem
            // 
            this.deleteChapterToolStripMenuItem.Enabled = false;
            this.deleteChapterToolStripMenuItem.Name = "deleteChapterToolStripMenuItem";
            this.deleteChapterToolStripMenuItem.Size = new System.Drawing.Size(225, 26);
            this.deleteChapterToolStripMenuItem.Text = "Удалить";
            this.deleteChapterToolStripMenuItem.Click += new System.EventHandler(this.DeleteChapterToolStripMenuItem_Click);
            // 
            // productsStripDropDownButton
            // 
            this.productsStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.productsStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addProductToolStripMenuItem,
            this.editProductToolStripMenuItem,
            this.deleteProductToolStripMenuItem,
            this.addToBasketToolStripMenuItem});
            this.productsStripDropDownButton.Enabled = false;
            this.productsStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("productsStripDropDownButton.Image")));
            this.productsStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.productsStripDropDownButton.Name = "productsStripDropDownButton";
            this.productsStripDropDownButton.Size = new System.Drawing.Size(65, 24);
            this.productsStripDropDownButton.Text = "Товар";
            // 
            // addProductToolStripMenuItem
            // 
            this.addProductToolStripMenuItem.Name = "addProductToolStripMenuItem";
            this.addProductToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.addProductToolStripMenuItem.Text = "Добавить";
            this.addProductToolStripMenuItem.Click += new System.EventHandler(this.AddProductToolStripMenuItem_Click);
            // 
            // editProductToolStripMenuItem
            // 
            this.editProductToolStripMenuItem.Name = "editProductToolStripMenuItem";
            this.editProductToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.editProductToolStripMenuItem.Text = "Изменить";
            this.editProductToolStripMenuItem.Click += new System.EventHandler(this.EditProductToolStripMenuItem_Click);
            // 
            // deleteProductToolStripMenuItem
            // 
            this.deleteProductToolStripMenuItem.Name = "deleteProductToolStripMenuItem";
            this.deleteProductToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.deleteProductToolStripMenuItem.Text = "Удалить";
            this.deleteProductToolStripMenuItem.Click += new System.EventHandler(this.DeleteProductToolStripMenuItem_Click);
            // 
            // addToBasketToolStripMenuItem
            // 
            this.addToBasketToolStripMenuItem.Name = "addToBasketToolStripMenuItem";
            this.addToBasketToolStripMenuItem.Size = new System.Drawing.Size(232, 26);
            this.addToBasketToolStripMenuItem.Text = "Добавить в корзину";
            this.addToBasketToolStripMenuItem.Click += new System.EventHandler(this.AddToBasketToolStripMenuItem_Click);
            // 
            // unselectStripButton
            // 
            this.unselectStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.unselectStripButton.Image = ((System.Drawing.Image)(resources.GetObject("unselectStripButton.Image")));
            this.unselectStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.unselectStripButton.Name = "unselectStripButton";
            this.unselectStripButton.Size = new System.Drawing.Size(160, 24);
            this.unselectStripButton.Text = "Сбросить выделение";
            this.unselectStripButton.Click += new System.EventHandler(this.UnselectStripButton_Click);
            // 
            // generatorToolStripButton
            // 
            this.generatorToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.generatorToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("generatorToolStripButton.Image")));
            this.generatorToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.generatorToolStripButton.Name = "generatorToolStripButton";
            this.generatorToolStripButton.Size = new System.Drawing.Size(119, 24);
            this.generatorToolStripButton.Text = "Сгенерировать";
            this.generatorToolStripButton.Click += new System.EventHandler(this.GeneratorToolStripButton_Click);
            // 
            // settingsStripButton
            // 
            this.settingsStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.settingsStripButton.Image = ((System.Drawing.Image)(resources.GetObject("settingsStripButton.Image")));
            this.settingsStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.settingsStripButton.Name = "settingsStripButton";
            this.settingsStripButton.Size = new System.Drawing.Size(88, 24);
            this.settingsStripButton.Text = "Настройки";
            this.settingsStripButton.Click += new System.EventHandler(this.SettingsStripButton_Click);
            // 
            // clientsToolStripButton
            // 
            this.clientsToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.clientsToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("clientsToolStripButton.Image")));
            this.clientsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clientsToolStripButton.Name = "clientsToolStripButton";
            this.clientsToolStripButton.Size = new System.Drawing.Size(172, 24);
            this.clientsToolStripButton.Text = "Список пользователей";
            this.clientsToolStripButton.Click += new System.EventHandler(this.ClientsToolStripButton_Click);
            // 
            // orderToolStripDropDownButton
            // 
            this.orderToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.orderToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.orderToolStripMenuItem,
            this.ordersListToolStripMenuItem,
            this.activeOrdersListToolStripMenuItem});
            this.orderToolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("orderToolStripDropDownButton.Image")));
            this.orderToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.orderToolStripDropDownButton.Name = "orderToolStripDropDownButton";
            this.orderToolStripDropDownButton.Size = new System.Drawing.Size(61, 24);
            this.orderToolStripDropDownButton.Text = "Заказ";
            // 
            // treeView
            // 
            this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeView.Location = new System.Drawing.Point(0, 28);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(468, 422);
            this.treeView.TabIndex = 1;
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView_AfterSelect);
            // 
            // listView
            // 
            this.listView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Название,
            this.Артикул,
            this.Количество,
            this.Цена});
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(465, 28);
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.ShowItemToolTips = true;
            this.listView.Size = new System.Drawing.Size(398, 422);
            this.listView.TabIndex = 2;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.ItemActivate += new System.EventHandler(this.ListView_ItemActivate);
            this.listView.SelectedIndexChanged += new System.EventHandler(this.ListView_SelectedIndexChanged);
            // 
            // Название
            // 
            this.Название.Text = "Название";
            this.Название.Width = 73;
            // 
            // Артикул
            // 
            this.Артикул.Text = "Артикул";
            this.Артикул.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Артикул.Width = 73;
            // 
            // Количество
            // 
            this.Количество.Text = "Количество";
            this.Количество.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Количество.Width = 73;
            // 
            // Цена
            // 
            this.Цена.Text = "Цена";
            this.Цена.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Цена.Width = 72;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // orderToolStripMenuItem
            // 
            this.orderToolStripMenuItem.Name = "orderToolStripMenuItem";
            this.orderToolStripMenuItem.Size = new System.Drawing.Size(269, 26);
            this.orderToolStripMenuItem.Text = "Оформить заказ";
            this.orderToolStripMenuItem.Click += new System.EventHandler(this.OrderToolStripButton_Click);
            // 
            // ordersListToolStripMenuItem
            // 
            this.ordersListToolStripMenuItem.Name = "ordersListToolStripMenuItem";
            this.ordersListToolStripMenuItem.Size = new System.Drawing.Size(269, 26);
            this.ordersListToolStripMenuItem.Text = "Список заказов";
            this.ordersListToolStripMenuItem.Click += new System.EventHandler(this.OrderListStripButton_Click);
            // 
            // activeOrdersListToolStripMenuItem
            // 
            this.activeOrdersListToolStripMenuItem.Name = "activeOrdersListToolStripMenuItem";
            this.activeOrdersListToolStripMenuItem.Size = new System.Drawing.Size(269, 26);
            this.activeOrdersListToolStripMenuItem.Text = "Список активных заказов";
            this.activeOrdersListToolStripMenuItem.Click += new System.EventHandler(this.activeOrdersListToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 450);
            this.Controls.Add(this.listView);
            this.Controls.Add(this.treeView);
            this.Controls.Add(this.toolStrip);
            this.Name = "MainForm";
            this.Text = "Kekich Warehouse";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripDropDownButton chapterStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem addChapterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editChapterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteChapterToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton productsStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem addProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton unselectStripButton;
        private System.Windows.Forms.ToolStripDropDownButton fileStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ColumnHeader Название;
        private System.Windows.Forms.ColumnHeader Артикул;
        private System.Windows.Forms.ColumnHeader Количество;
        private System.Windows.Forms.ColumnHeader Цена;
        private System.Windows.Forms.ToolStripButton settingsStripButton;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        public System.Windows.Forms.TreeView treeView;
        public System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ToolStripMenuItem addToRootToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton generatorToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem addToBasketToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton clientsToolStripButton;
        private System.Windows.Forms.ToolStripDropDownButton orderToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem orderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ordersListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activeOrdersListToolStripMenuItem;
    }
}

