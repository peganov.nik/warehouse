﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Класс, в котором хранится вся информация о товаре.
    /// </summary>
    [DataContract]
    public class Product
    {
        /// <summary>
        /// Имя товара.
        /// </summary>
        [DataMember]
        public string name;
        /// <summary>
        /// Артикул товара.
        /// </summary>
        [DataMember]
        public string article;
        /// <summary>
        /// Оставшееся на складе количество товара.
        /// </summary>
        [DataMember]
        public uint count;
        /// <summary>
        /// Цена товара.
        /// </summary>
        [DataMember]
        public double price;
        /// <summary>
        /// Элемент ListView, соответствующий данному товару.
        /// </summary>
        public ListViewItem item;
        /// <summary>
        /// Список всех заканчивающихся товаров.
        /// </summary>
        public static List<ProductToSave> outOfStockProducts;
        /// <summary>
        /// Обновляет список заканчивающихся товаров.
        /// </summary>
        public static List<ProductToSave> OutOfStock 
        {
            get
            {
                outOfStockProducts = new List<ProductToSave>();
                // Рекурсивно обходим все разделы и добавляем товары из них.
                foreach (var chapter in MainForm.baseChapters)
                {
                    chapter.AddOutOfStock("");
                }
                return outOfStockProducts;
            }
        }
        /// <summary>
        /// Проверяет, существует ли в системе товар с данным артиклем.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        internal static bool CheckSame(string article)
        {
            foreach (var chapter in MainForm.baseChapters)
            {
                // Производим поиск по всем продуктам всех подразделов корневых разделов.
                // null - значение по умолчанию ссылочного типа Product.
                if (chapter.SubProducts(new List<Product>(), false).Find((Product p) => (p.article == article)) != null)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Создает объект товара по переданному элементу ListView.
        /// </summary>
        /// <param name="item">Соответсвующий товару элемент ListView.</param>
        public Product(ListViewItem item)
        {
            name = item.SubItems[0].Text;
            article = item.SubItems[1].Text;
            count = uint.Parse(item.SubItems[2].Text);
            price = double.Parse(item.SubItems[3].Text);
            this.item = item;
        }
        /// <summary>
        /// Создает объект товара по значениям параметров.
        /// </summary>
        /// <param name="name">Имя товара.</param>
        /// <param name="article">Артикул товара.</param>
        /// <param name="count">Количество оставшегося товара.</param>
        /// <param name="price">Цена товара.</param>
        public Product(string name, string article, uint count, double price)
        {
            this.name = name;
            this.article = article;
            this.count = count;
            this.price = price;
        }
        /// <summary>
        /// Генерирует список случайных продуктов.
        /// </summary>
        /// <param name="productsCount">Нужное количество продуктов.</param>
        /// <param name="randomGenerator">Объект класса Random для исключения одинаковых значений.</param>
        /// <param name="all">Если значение параметра true, генерируется список из productsCount товаров, иначе случайное число.</param>
        /// <returns></returns>
        public static List<Product> GenerateRandomProducts(ref uint productsCount, Random randomGenerator, bool all = false)
        {
            var res = new List<Product>();
            // Число товаров в списке.
            int currentProductsCount = randomGenerator.Next((int)productsCount + 1);
            for (int i = 0; i < currentProductsCount; ++i)
            {
                // Создание нового товара с случайными значениями.
                res.Add(new Product(Chapter.RandomName(randomGenerator),
                    Chapter.RandomName(randomGenerator),
                    (uint)randomGenerator.Next(100),
                    randomGenerator.NextDouble() * 5000));
            }
            // Изменение оставшегося числа продуктов (передается по ссылке).
            productsCount -= (uint)currentProductsCount;
            if (all)
            {
                // Генерация оставшихся товаров, если нужно.
                for (int i = 0; i < productsCount; ++i)
                {
                    res.Add(new Product(Chapter.RandomName(randomGenerator),
                        Chapter.RandomName(randomGenerator),
                        (uint)randomGenerator.Next(100),
                        randomGenerator.NextDouble() * 5000));
                }
                productsCount = 0;
            }
            return res;
        }
    }
    /// <summary>
    /// Класс со всеми свойствами товара, что нужно сохранить в отчёт.
    /// </summary>
    public class ProductToSave
    {
        /// <summary>
        /// Путь к данному товару, то есть строка из всех разделов через /.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Имя сохраняемого товара.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Артикул сохраняемого товара.
        /// </summary>
        public string Article { get; set; }
        /// <summary>
        /// Количество сохраняемого товара на складе.
        /// </summary>
        public uint Count { get; set; }
        /// <summary>
        /// Создает объект сохраняемого товара по переданным параметрам.
        /// </summary>
        /// <param name="path">Путь к товару.</param>
        /// <param name="name">Имя товара.</param>
        /// <param name="article">Артикул товара.</param>
        /// <param name="count">Оставшееся количество товара.</param>
        public ProductToSave(string path, string name, string article, uint count)
        {
            Path = path;
            Name = name;
            Article = article;
            Count = count;
        }
    }
}
