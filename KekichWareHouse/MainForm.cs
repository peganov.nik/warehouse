﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Security;
using System.Text;
using System.Windows.Forms;

namespace KekichWareHouse
{
    public partial class MainForm : Form
    {
        /// <summary>
        /// Список всех корневых разделов.
        /// </summary>
        public static List<Chapter> baseChapters;
        /// <summary>
        /// Словарь, ставящий объект раздела в соответствие с элеметном в древовидной структуре.
        /// </summary>
        public static Dictionary<TreeNode, Chapter> findChapter;
        /// <summary>
        /// Имя файла, в который был сохранён файл или из которого был открыт.
        /// Нужно для нажатия кнопки "Сохранить".
        /// </summary>
        string fileToSave = null;
        /// <summary>
        /// Число, являющееся границей "заканчиваемости" товара.
        /// </summary>
        public static uint outOfStockCount = 5;
        /// <summary>
        /// Тип авторизации пользователя.
        /// </summary>
        public static AuthorizationType authorizationType = AuthorizationType.NoAccess;
        /// <summary>
        /// Выбранный пользователь.
        /// </summary>
        public static User currentUser = null;
        public MainForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Добавление нового раздела.
        /// </summary>
        private void AddChapterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditChapterForm editChapterForm = new EditChapterForm(EditChapterForm.ChapterFormType.Add)
            {
                Owner = this
            };
            editChapterForm.ShowDialog();
        }
        /// <summary>
        /// Редактирование раздела.
        /// </summary>
        private void EditChapterToolStripMenuItem(object sender, EventArgs e)
        {
            EditChapterForm editChapterForm = new EditChapterForm(EditChapterForm.ChapterFormType.Edit)
            {
                Owner = this
            };
            editChapterForm.ShowDialog();
        }
        /// <summary>
        /// Обработчик нажатия кнопки Снять выделение.
        /// </summary>
        private void UnselectStripButton_Click(object sender, EventArgs e)
        {
            treeView.SelectedNode = null;
            listView.Items.Clear();
            editChapterToolStripMenuItem.Enabled = false;
            deleteChapterToolStripMenuItem.Enabled = false;
            productsStripDropDownButton.Enabled = false;
        }
        /// <summary>
        /// Метод, который вызывается при выборе нового элемента древовидной структуры.
        /// Прорисовывает все товары раздела, если есть.
        /// </summary>
        private void TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            editChapterToolStripMenuItem.Enabled = true;
            deleteChapterToolStripMenuItem.Enabled = true;
            productsStripDropDownButton.Enabled = true;
            // Проверка, есть ли у раздела подразделы.
            if (treeView.SelectedNode.Nodes.Count > 0)
            {
                deleteChapterToolStripMenuItem.Enabled = false;
            }
            else
            {
                deleteChapterToolStripMenuItem.Enabled = true;
            }
            RedrawOwnList();
            RedrawSubList();
        }
        /// <summary>
        /// Перерисовывает список товаров.
        /// Показывает только собственные товары раздела (жирным шрифтом).
        /// </summary>
        private void RedrawOwnList()
        {
            listView.Items.Clear();
            foreach (var product in findChapter[treeView.SelectedNode].products)
            {
                ListViewItem newItem = new ListViewItem(product.name, 0);
                newItem.Font = new Font(newItem.Font, FontStyle.Bold);
                // Подсказка при наведении на товар.
                newItem.ToolTipText = "Собственный товар раздела.";
                newItem.SubItems.Add(product.article);
                newItem.SubItems.Add($"{product.count}");
                newItem.SubItems.Add($"{product.price}");
                listView.Items.Add(newItem);
            }
        }
        /// <summary>
        /// Перерисовывает список товаров подразделов.
        /// Показывает только товары подразделов (обычным шрифтом).
        /// </summary>
        private void RedrawSubList()
        {
            foreach (var product in findChapter[treeView.SelectedNode].SubProducts(new List<Product>()))
            {
                ListViewItem newItem = new ListViewItem(product.name, 0);
                // Подсказка при наведении на товар.
                newItem.ToolTipText = "Товар подраздела.";
                newItem.SubItems.Add(product.article);
                newItem.SubItems.Add($"{product.count}");
                newItem.SubItems.Add($"{product.price}");
                listView.Items.Add(newItem);
            }
        }
        /// <summary>
        /// Удаление раздела.
        /// </summary>
        private void DeleteChapterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var currentNode = treeView.SelectedNode;
            var currentChapter = findChapter[currentNode];
            // Проверка на наличие у раздела подразделов.
            if (currentNode.Nodes.Count > 0)
            {
                MessageBox.Show("Невозможно удалить раздел с потомками.", "Предупреждение");
                return;
            }
            treeView.BeginUpdate();
            if (MessageBox.Show("Вы уверены, что хотите удалить выделенный раздел?",
                "Предупреждение", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    if (currentNode.Parent != null)
                    {
                        findChapter[currentNode.Parent].sons.Remove(currentChapter);
                    }
                    treeView.Nodes.Remove(treeView.SelectedNode);
                }
                catch (KeyNotFoundException)
                {
                    MessageBox.Show("Выделенный раздел не найден в системе. Попробуйте ещё раз.", "Ошибка");
                }
                catch
                {
                    MessageBox.Show("Необработанное исключение.", "Ошибка");
                }
            }
            treeView.EndUpdate();
        }
        /// <summary>
        /// Добавление нового продукта.
        /// </summary>
        private void AddProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditProductForm editGoodForm = new EditProductForm(EditProductForm.ProductFormType.Add) 
            { 
                Owner = this 
            };
            editGoodForm.ShowDialog();
        }
        /// <summary>
        /// Загрузка формы.
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Запускаем форму авторизации.
            AuthorizationForm authorizationForm = new AuthorizationForm()
            {
                Owner = this
            };
            authorizationForm.ShowDialog();
            if (authorizationType == AuthorizationType.NoAccess)
            {
                // Пользователь не авторизировался, программа закрывается.
                Close();
            }
            // Создание начальных значений переменных.
            baseChapters = new List<Chapter>();
            findChapter = new Dictionary<TreeNode, Chapter>();
            listView.View = View.Details;
            listView.GridLines = true;
            // Скрываем недоступные кнопки для пользователя.
            if (authorizationType == AuthorizationType.User)
            {
                chapterStripDropDownButton.Visible = false;
                addProductToolStripMenuItem.Visible = false;
                editProductToolStripMenuItem.Visible = false;
                generatorToolStripButton.Visible = false;
                settingsStripButton.Visible = false;
                deleteProductToolStripMenuItem.Visible = false;
                saveAsToolStripMenuItem.Visible = false;
                saveToolStripMenuItem.Visible = false;
                reportToolStripMenuItem.Visible = false;
                clientsToolStripButton.Visible = false;
                activeOrdersListToolStripMenuItem.Visible = false;
            }
            else
            {
                // Скрываем недоступные кнопки для администратора.
                orderToolStripMenuItem.Visible = false;
            }
            Order.LoadOrders();
        }
        /// <summary>
        /// Прорисовка древовидной структуры при открытии файла.
        /// </summary>
        public void DrawTreeView()
        {
            treeView.Nodes.Clear();
            foreach (var chapter in baseChapters)
            {
                chapter.Add(treeView.Nodes);
            }
        }
        /// <summary>
        /// Редактирование товара.
        /// </summary>
        private void EditProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditProductForm editGoodForm = new EditProductForm(EditProductForm.ProductFormType.Edit)
            {
                Owner = this
            };
            editGoodForm.ShowDialog();
        }
        /// <summary>
        /// Нажатие на один из товаров.
        /// </summary>
        private void ListView_ItemActivate(object sender, EventArgs e)
        {
            productsStripDropDownButton.Enabled = true;
            addProductToolStripMenuItem.Enabled = false;
        }
        /// <summary>
        /// Нажатие на другой товар.
        /// </summary>
        private void ListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            productsStripDropDownButton.Enabled = true;
        }
        /// <summary>
        /// Закрытие формы с возможностью сохранения информации.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Изменить, а значит, сохранить склад может только администратор.
            if (authorizationType == AuthorizationType.Administrator)
            {
                if (MessageBox.Show("Сохранить файл перед закрытием?", "Закрытие", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    // Указание параметров SaveFileDialog'а.
                    saveFileDialog.Filter = "serialization files (*.ser)|*.ser";
                    saveFileDialog.FilterIndex = 2;
                    saveFileDialog.RestoreDirectory = true;
                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        SaveInformation();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
            }
            Order.SaveOrders();
        }
        /// <summary>
        /// Сохранение всей информации в выбранный файл.
        /// </summary>
        private void SaveInformation()
        {
            Stream myStream;
            try
            {
                if ((myStream = saveFileDialog.OpenFile()) != null)
                {
                    using (myStream)
                    {
                        // Сериализация baseChapters.
                        DataContractJsonSerializer format = new DataContractJsonSerializer(typeof(List<Chapter>));
                        format.WriteObject(myStream, baseChapters);
                        fileToSave = saveFileDialog.FileName;
                        saveToolStripMenuItem.Enabled = true;
                    }
                }
            }
            catch (Exception ex) when (ex is SecurityException || ex is AccessViolationException)
            {
                MessageBox.Show("Вы не имеете прав доступа для сохранения файла. Попробуйте другую директорию.", "Ошибка");
            }
            catch (IOException)
            {
                MessageBox.Show("Ошибка диска. Закройте файл, если он открыт.", "Ошибка");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Полученный склад невозможно записать. Измените и попробуйте снова.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Сохранение склада с возможностью выбрать директорию и имя файла.
        /// </summary>
        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Указание параметров SaveFileDialog'а.
            saveFileDialog.Filter = "serialization files (*.ser)|*.ser";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                SaveInformation();
            }
        }
        /// <summary>
        /// Сохранение файла в его прошлую директорию.
        /// </summary>
        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                // Сериализация всей информации.
                DataContractJsonSerializer format = new DataContractJsonSerializer(typeof(List<Chapter>));
                Stream saveStream = File.Create(fileToSave);
                format.WriteObject(saveStream, baseChapters);
                saveStream.Close();
            }
            catch (Exception ex) when (ex is SecurityException || ex is AccessViolationException)
            {
                MessageBox.Show("Вы не имеете прав доступа для создания файла. Попробуйте другую директорию.", "Ошибка");
            }
            catch (IOException)
            {
                MessageBox.Show("Ошибка диска. Закройте файл, если он открыт.", "Ошибка");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Полученный склад невозможно записать. Измените и попробуйте снова.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Открытие склада из файла.
        /// </summary>
        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "serialization files (*.ser)|*.ser";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                ReadInformation();
                DrawTreeView();
            }
        }
        /// <summary>
        /// Чтение всей информации о складе из уже выбранного файла.
        /// </summary>
        private void ReadInformation()
        {
            DataContractJsonSerializer format = new DataContractJsonSerializer(typeof(List<Chapter>));
            try
            {
                using (var fileStream = openFileDialog.OpenFile())
                {
                    // Десериализация склада.
                    baseChapters = (List<Chapter>)format.ReadObject(fileStream);
                    // Запоминание имени файла, чтобы можно было использовать кнопку "сохранить".
                    fileToSave = openFileDialog.FileName;
                    saveToolStripMenuItem.Enabled = true;
                }
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Выбранный файл — null.", "Ошибка");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Файл повреждён и не может быть открыт.", "Ошибка");
            }
            catch (InvalidCastException)
            {
                MessageBox.Show("Файл не является сладом.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Открытие окна настроек.
        /// </summary>
        private void SettingsStripButton_Click(object sender, EventArgs e)
        {
            SettingsForm settings = new SettingsForm();
            settings.ShowDialog();
        }
        /// <summary>
        /// Создание отчёта.
        /// </summary>
        private void ReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv";
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                SafeSaveReport();
            }
        }
        /// <summary>
        /// Безопасное сохранение отчёта с обработчиком ошибок.
        /// </summary>
        private void SafeSaveReport()
        {
            try
            {
                UnsafeSaveReport();
            }
            catch (Exception ex) when (ex is SecurityException || ex is AccessViolationException)
            {
                MessageBox.Show("Вы не имеете прав доступа для создания файла. Попробуйте другую директорию.", "Ошибка");
            }
            catch (IOException)
            {
                MessageBox.Show("Ошибка диска. Закройте файл, если он открыт.", "Ошибка");
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Выбранный Вами файл — null.", "Ошибка");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Файл не может быть открыт на запись.", "Ошибка");
            }
            catch (ConfigurationException)
            {
                MessageBox.Show("В выбранном файле невозможна принятая в программе конфигурация.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Небезопасное сохранение отчёта, возможны ошибки.
        /// </summary>
        private void UnsafeSaveReport()
        {
            Stream myStream;
            if ((myStream = saveFileDialog.OpenFile()) != null)
            {
                using (myStream)
                {
                    var config = new CsvConfiguration(CultureInfo.CurrentCulture) { Delimiter = "," };
                    using (StreamWriter streamWriter = new StreamWriter(myStream, Encoding.UTF8))
                    using (CsvWriter csvWriter = new CsvWriter(streamWriter, config))
                    {
                        // Записывает обновленный список заканчивающихся товаров.
                        csvWriter.WriteRecords(Product.OutOfStock);
                    }
                }
            }
        }
        /// <summary>
        /// Удаление продукта.
        /// </summary>
        private void DeleteProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите удалить выделенный товар?",
                "Предупреждение", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    findChapter[treeView.SelectedNode].Remove(listView.SelectedItems[0]);
                    listView.SelectedItems[0].Remove();
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show("Вы не выбрали товар. Попробуйте ещё раз.", "Ошибка");
                }
                catch (ArgumentNullException)
                {
                    MessageBox.Show("Раздел не выбран. Попробуйте ещё раз.", "Ошибка");
                }
                catch (KeyNotFoundException)
                {
                    MessageBox.Show("Выделенный раздел не найден в системе. Попробуйте ещё раз.", "Ошибка");
                }
                catch
                {
                    MessageBox.Show("Необработанное исключение.", "Ошибка");
                }
            }
        }
        /// <summary>
        /// Добавление раздела в корень.
        /// </summary>
        private void AddToRootToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditChapterForm editChapterForm = new EditChapterForm(EditChapterForm.ChapterFormType.AddToRoot)
            {
                Owner = this
            };
            editChapterForm.ShowDialog();
        }
        /// <summary>
        /// Метод, вызываемый при нажатии кнопки Сегенировать автоматически.
        /// Открывает окно настроек автоматической генерации.
        /// </summary>
        private void GeneratorToolStripButton_Click(object sender, EventArgs e)
        {
            GeneratorForm generatorForm = new GeneratorForm()
            {
                Owner = this
            };
            generatorForm.ShowDialog();
        }
        /// <summary>
        /// Добавление в корзину выделенного товара.
        /// </summary>
        private void AddToBasketToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var currentNode = treeView.SelectedNode;
                var currentChapter = findChapter[currentNode];
                currentUser?.basket.Add(currentChapter.products[listView.SelectedIndices[0]]);
            }
            catch (Exception ex) when (ex is NullReferenceException || ex is IndexOutOfRangeException)
            {
                MessageBox.Show("Вы не выбрали товар. Попробуйте ещё раз.", "Ошибка");
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Раздел не выбран. Попробуйте ещё раз.", "Ошибка");
            }
            catch (KeyNotFoundException)
            {
                MessageBox.Show("Выделенный раздел не найден в системе. Попробуйте ещё раз.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Нажатие на кнопку "Оформление заказа".
        /// </summary>
        private void OrderToolStripButton_Click(object sender, EventArgs e)
        {
            if (currentUser.basket.Count == 0)
            {
                MessageBox.Show("Корзина пуста!", "Ошибка!");
            }
            else if (MessageBox.Show($"Вы уверены, что хотите заказать {string.Join(", ", from product in currentUser.basket select product.name)}?",
                "Оформление заказа", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Order.orders.Add(new Order(new List<Product>(currentUser.basket), currentUser));
                currentUser.basket.Clear();
            }
        }

        private void OrderListStripButton_Click(object sender, EventArgs e)
        {
            var orderForm = new OrdersForm();
            orderForm.ShowDialog();
        }

        private void ClientsToolStripButton_Click(object sender, EventArgs e)
        {
            var clientForm = new ClientsForm();
            clientForm.ShowDialog();
        }

        private void activeOrdersListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var orderForm = new OrdersForm(true);
            orderForm.ShowDialog();
        }
    }
}
