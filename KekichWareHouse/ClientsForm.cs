﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Форма отображения списка клиентов.
    /// </summary>
    public partial class ClientsForm : Form
    {
        public ClientsForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Метод загрузки формы. Прорисовка listView.
        /// </summary>
        private void ClientsForm_Load(object sender, EventArgs e)
        {
            listView.View = View.Details;
            listView.GridLines = true;
            foreach (var user in User.users)
            {
                // Последовательно добавляем пользователей.
                ListViewItem newItem = new ListViewItem($"{user.email}", 0);
                newItem.Font = new Font(newItem.Font, FontStyle.Bold);
                // Подсказка при наведении на заказ.
                newItem.ToolTipText = "Нажмите на клиента, чтобы увидеть список заказов клиента.";
                // Используем LINQ для быстрого вычисления.
                //newItem.SubItems.Add($"{(from o in Order.orders where o.user.email == user.email && (int)(o.status & OrderStatus.Paid) == (int)OrderStatus.Paid select o.Sum()).Sum()}");
                double sum = 0;
                foreach (Order order in Order.orders)
                {
                    if (order.user.email == user.email && (int)(order.status & OrderStatus.Paid) == (int)OrderStatus.Paid)
                    {
                        sum += order.Sum();
                    }
                }
                newItem.SubItems.Add($"{sum}");
                listView.Items.Add(newItem);
            }
        }
    }
}
