﻿using System;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Форма изменения настроек.
    /// </summary>
    public partial class SettingsForm : Form
    {
        /// <summary>
        /// Создание формы.
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Загрузка формы настроек.
        /// </summary>
        private void SettingsForm_Load(object sender, EventArgs e)
        {
            outOfStockTextBox.Text = $"{MainForm.outOfStockCount}";
        }
        /// <summary>
        /// Нажатие кнопки Отмена.
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// Наатие кнопки Ок.
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            // Проверка введённой информации.
            if (!uint.TryParse(outOfStockTextBox.Text, out MainForm.outOfStockCount))
            {
                MessageBox.Show("Значение поля должно быть неотрицательным числом!",
                    "Ошибка");
            }
            else
            {
                Close();
            }
        }
    }
}
