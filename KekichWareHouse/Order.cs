﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Статус хранения заказа.
    /// Работает с помощью побитового или.
    /// </summary>
    [Flags]
    public enum OrderStatus
    {
        Processed = 1,
        Paid = 2,
        Shipped = 4,
        Executed = 8
    }
    /// <summary>
    /// Класс для хранения информации о заказах.
    /// </summary>
    [DataContract]
    public class Order
    {
        /// <summary>
        /// Список продуктов.
        /// </summary>
        [DataMember]
        public List<Product> products;
        /// <summary>
        /// Идентификационный номер каждого заказа.
        /// </summary>
        [DataMember]
        public int id;
        /// <summary>
        /// Ссылка на пользователя, который оформил заказ.
        /// </summary>
        [DataMember]
        public User user;
        /// <summary>
        /// Статус заказа.
        /// </summary>
        [DataMember]
        public OrderStatus status;
        /// <summary>
        /// Дата и время оформления заказа.
        /// </summary>
        [DataMember]
        public DateTime dateTime;
        /// <summary>
        /// Список всех заказов.
        /// </summary>
        public static List<Order> orders;
        /// <summary>
        /// Конструктор для создания заказа из списка продуктов.
        /// </summary>
        /// <param name="products">Список продуктов.</param>
        /// <param name="user">Ссылка на создателя заказа.</param>
        public Order(List<Product> products, User user)
        {
            this.products = products;
            id = orders.Count;
            // Текущее время.
            dateTime = DateTime.Now;
            this.user = user;
        }
        /// <summary>
        /// Загрузка заказов из файла.
        /// </summary>
        public static void LoadOrders()
        {
            try
            {
                DataContractJsonSerializer format = new DataContractJsonSerializer(typeof(List<Order>));
                orders = new List<Order>();
                using (var fileStream = new FileStream("orders.json", FileMode.Open))
                {
                    // Десериализация списка заказов.
                    orders = (List<Order>)format.ReadObject(fileStream);
                }
            }
            // Обработка потенциальных исключений.
            catch (ArgumentNullException)
            {
                MessageBox.Show("Выбранный файл — null.", "Ошибка");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Файл повреждён и не может быть открыт.", "Ошибка");
            }
            catch (InvalidCastException)
            {
                MessageBox.Show("Файл не является списком заказов.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Сохранение всех заказов.
        /// </summary>
        public static void SaveOrders()
        {
            Stream myStream;
            try
            {
                if ((myStream = new FileStream("orders.json", FileMode.OpenOrCreate)) != null)
                {
                    using (myStream)
                    {
                        // Сериализация orders.
                        DataContractJsonSerializer format = new DataContractJsonSerializer(typeof(List<Order>));
                        format.WriteObject(myStream, orders);
                    }
                }
            }
            // Обработка потенциальных исключений.
            catch (Exception ex) when (ex is SecurityException || ex is AccessViolationException)
            {
                MessageBox.Show("Вы не имеете прав доступа для сохранения файла. Попробуйте другую директорию.", "Ошибка");
            }
            catch (IOException)
            {
                MessageBox.Show("Ошибка диска. Закройте файл, если он открыт.", "Ошибка");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Полученный склад невозможно записать. Измените и попробуйте снова.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Сумма цен всех товаров заказа.
        /// </summary>
        /// <returns>Сумма.</returns>
        public double Sum()
        {
            double res = 0;
            foreach (var product in products)
            {
                res += product.price;
            }
            return res;
        }
    }
}
