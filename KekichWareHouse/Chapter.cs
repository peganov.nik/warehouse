﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Класс, в котором хранится вся информация о каждом разделе.
    /// </summary>
    [DataContract]
    public class Chapter
    {
        /// <summary>
        /// Имя раздела.
        /// </summary>
        [DataMember]
        public string name;
        /// <summary>
        /// Список всех потомков раздела.
        /// </summary>
        [DataMember]
        public List<Chapter> sons;
        /// <summary>
        /// Список всех товаров в разделе.
        /// </summary>
        [DataMember]
        public List<Product> products;
        public Chapter(string name)
        {
            this.name = name;
            sons = new List<Chapter>();
            products = new List<Product>();
        }
        /// <summary>
        /// Удаляет товар из раздела.
        /// </summary>
        /// <param name="listViewItem">Товар, который нужно удалить.</param>
        internal void Remove(ListViewItem listViewItem)
        {
            foreach (var product in products)
            {
                if (product.item == listViewItem)
                {
                    products.Remove(product);
                    break;
                }
            }
        }
        /// <summary>
        /// Генерация случайного списка разделов.
        /// </summary>
        /// <param name="chaptersCount">Число оставшихся разделов, которые нужно сгенерировать.</param>
        /// <param name="productsCount">Число оставшихся продуктов, которые нужно сгенерировать.</param>
        /// <param name="randomGenerator">Объект класса Random для исключения одинаковых значений.</param>
        /// <param name="all">Если значение параметра true, генерируется список из chaptersCount разделов, иначе случайное число.</param>
        /// <returns>Случайный список разделов.</returns>
        internal static List<Chapter> Generate(ref uint chaptersCount, ref uint productsCount, Random randomGenerator, bool all)
        {
            // Генерация случайного числа объектов.
            List<Chapter> res = new List<Chapter>();
            // Здесь chaptersCount это не баг, нужно оставить хотя бы один раздел для отображения всех товаров.
            int currentChaptersCount = randomGenerator.Next((int)chaptersCount);
            chaptersCount -= (uint)currentChaptersCount;
            for (int i = 0; i < currentChaptersCount; ++i)
            {
                var nextChapter = new Chapter(RandomName(randomGenerator));
                nextChapter.sons = Generate(ref chaptersCount, ref productsCount, randomGenerator, false);
                nextChapter.products = Product.GenerateRandomProducts(ref productsCount, randomGenerator);
                res.Add(nextChapter);
            }
            // Генерация оставшихся объектов.
            // Генерируется хотя бы один, чтобы показать все товары.
            if (all)
            {
                for (int i = 0; i < chaptersCount - 1; ++i)
                {
                    var endChapter = new Chapter(RandomName(randomGenerator));
                    endChapter.sons =  new List<Chapter>();
                    endChapter.products = Product.GenerateRandomProducts(ref productsCount, randomGenerator);
                    res.Add(endChapter);
                }
                var lastChapter = new Chapter(RandomName(randomGenerator));
                lastChapter.sons = new List<Chapter>();
                lastChapter.products = Product.GenerateRandomProducts(ref productsCount, randomGenerator, true);
                res.Add(lastChapter);
                chaptersCount = 0;
            }
            return res;
        }
        /// <summary>
        /// Генерация случайного имени раздела или товара.
        /// </summary>
        /// <param name="randomGenerator">Объект класса Random для избежания повторения.</param>
        /// <returns>Случайная строка.</returns>
        public static string RandomName(Random randomGenerator)
        {
            string s = "";
            for (int i = 0; i < randomGenerator.Next(5, 15); ++i)
            {
                s += (char)('a' + randomGenerator.Next(28));
            }
            return s;
        }

        /// <summary>
        /// Рекурсивно обходит все разделы и собирает из них все заканчивающиеся товары.
        /// </summary>
        /// <param name="currentPath">Путь к текущему разделу.</param>
        public void AddOutOfStock(string currentPath)
        {
            // Обход товаров текущего раздела.
            foreach (var product in products)
            {
                if (product.count <= MainForm.outOfStockCount)
                {
                    try
                    {
                        Product.outOfStockProducts.Add(new ProductToSave(currentPath,
                            product.name, product.article, product.count));
                    }
                    catch (IndexOutOfRangeException)
                    {
                        MessageBox.Show("Программа не предназначена для параллельного запуска. Закройте другое окно.", "Ошибка");
                    }
                    catch
                    {
                        MessageBox.Show("Необработанное исключение.", "Ошибка");
                    }
                }
            }
            // Рекурсивный обход всех потомков.
            foreach (var son in sons)
            {
                son.AddOutOfStock($"{currentPath}/{name}");
            }
        }
        /// <summary>
        /// Добавляет текущий раздел и всех его наследников в переданную коллекцию элементов TreeNode.
        /// </summary>
        /// <param name="nodes">Ссылка на коллекцию, в которую нужно добавить раздел.</param>
        internal void Add(TreeNodeCollection nodes)
        {
            try { 
                nodes.Add(name);
                // Добавление в словарь для дальнейшего поиска.
                MainForm.findChapter[nodes[nodes.Count - 1]] = this;
                // Рекурсивное добавление наследников.
                foreach (var son in sons)
                {
                    son.Add(nodes[nodes.Count - 1].Nodes);
                }
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("Программа не предназначена для параллельного запуска. Закройте другое окно.", "Ошибка");
            }
            catch (StackOverflowException)
            {
                MessageBox.Show("Слишком глубокий уровень вложения.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Список продуктов всех подразделов данного раздела.
        /// </summary>
        /// <param name="currentProducts">Текущий список продуктов.</param>
        /// <param name="isFirst">Является ли текущий раздел первым в порядке обхода.</param>
        /// <returns>Список продуктов всех подразделов.</returns>
        public List<Product> SubProducts(List<Product> currentProducts, bool isFirst=true)
        {
            if (!isFirst)
            {
                // Обход всех продуктов текущего раздела.
                foreach (var product in products)
                {
                    try
                    {
                        currentProducts.Add(product);
                    }
                    catch (IndexOutOfRangeException)
                    {
                        MessageBox.Show("Программа не предназначена для параллельного запуска. Закройте другое окно.", "Ошибка");
                    }
                    catch (StackOverflowException)
                    {
                        MessageBox.Show("Слишком глубокий уровень вложения.", "Ошибка");
                    }
                    catch
                    {
                        MessageBox.Show("Необработанное исключение.", "Ошибка");
                    }
                }
            }
            // Рекурскивный обход подразделов.
            foreach (var son in sons)
            {
                son.SubProducts(currentProducts, false);
            }
            return currentProducts;
        }
    }
}
