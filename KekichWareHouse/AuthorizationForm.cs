﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Тип доступа к приложению.
    /// </summary>
    public enum AuthorizationType
    {
        /// <summary>
        /// Авторизация в качестве администратора.
        /// </summary>
        Administrator,
        /// <summary>
        /// Авторизация в качестве пользователя.
        /// </summary>
        User,
        /// <summary>
        /// В случае отмены авторизации.
        /// </summary>
        NoAccess
    }
    /// <summary>
    /// Форма авторизации пользователя.
    /// </summary>
    public partial class AuthorizationForm : Form
    {
        /// <summary>
        /// Логин администратора.
        /// </summary>
        private const string adminMail = "admin";
        /// <summary>
        /// Пароль администратора.
        /// </summary>
        private const string adminPassword = "admin";

        public AuthorizationForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Нажатие на кнопку "Отмена". Выход из приложения.
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// Нажатие на кнопку "Войти". Проверка логина и пароля.
        /// </summary>
        private void EnterButton_Click(object sender, EventArgs e)
        {
            if (emailBox.Text == adminMail && passwordBox.Text == adminPassword)
            {
                // Авторизация администратора.
                MainForm.authorizationType = AuthorizationType.Administrator;
                Close();
                return;
            }
            // Создание списка пользователей с таким e-mail (в списке всегда один элемент).
            IEnumerable<User> validUsers = from user in User.users where user?.email == emailBox.Text select user;
            foreach (User user in validUsers) {
                // Проверка на соответствие пароля.
                if (user.password == passwordBox.Text)
                {
                    MainForm.authorizationType = AuthorizationType.User;
                    MainForm.currentUser = user;
                    Close();
                    return;
                }
                else
                {
                    MessageBox.Show("Пароль не верен.", "Ошибка!");
                }
            }
            MessageBox.Show("Пользователь не найден.", "Ошибка!");
        }
        /// <summary>
        /// Загрузка формы.
        /// Загрузка базы данных юзеров.
        /// </summary>
        private void AuthorizationForm_Load(object sender, EventArgs e)
        {
            User.users = new User[0];
            DataContractJsonSerializer format = new DataContractJsonSerializer(typeof(User[]));
            try
            {
                using (var fileStream = File.Open("users.json", FileMode.OpenOrCreate))
                {
                    // Десериализация базы данных пользователей.
                    User.users = (User[])format.ReadObject(fileStream);
                }
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Выбранный файл — null.", "Ошибка");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Файл повреждён и не может быть открыт.", "Ошибка");
            }
            catch (InvalidCastException)
            {
                MessageBox.Show("Файл не является базой данных пользователей.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
        /// <summary>
        /// Обработчик нажатия на кнопку "Зарегистрироваться".
        /// </summary>
        private void RegistrationButton_Click(object sender, EventArgs e)
        {
            // Открытие формы регистрации.
            RegistrationForm registration = new RegistrationForm()
            {
                Owner = this
            };
            registration.ShowDialog();
        }
        /// <summary>
        /// Обработчик закрытия формы авторизации. Сохранение данных о пользователях.
        /// </summary>
        private void AuthorizationForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Stream myStream;
            try
            {
                if ((myStream = new FileStream("users.json", FileMode.OpenOrCreate)) != null)
                {
                    using (myStream)
                    {
                        // Сериализация users.
                        DataContractJsonSerializer format = new DataContractJsonSerializer(typeof(User[]));
                        format.WriteObject(myStream, User.users);
                    }
                }
            }
            catch (Exception ex) when (ex is SecurityException || ex is AccessViolationException)
            {
                MessageBox.Show("Вы не имеете прав доступа для сохранения файла. Попробуйте другую директорию.", "Ошибка");
            }
            catch (IOException)
            {
                MessageBox.Show("Ошибка диска. Закройте файл, если он открыт.", "Ошибка");
            }
            catch (SerializationException)
            {
                MessageBox.Show("Полученный склад невозможно записать. Измените и попробуйте снова.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
        }
    }
}
