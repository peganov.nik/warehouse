﻿
namespace KekichWareHouse
{
    partial class OrdersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView = new System.Windows.Forms.ListView();
            this.indexColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.processedColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.paidColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.shippedColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.executedColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.indexColumn,
            this.processedColumn,
            this.paidColumn,
            this.shippedColumn,
            this.executedColumn});
            this.listView.HideSelection = false;
            this.listView.Location = new System.Drawing.Point(-3, 2);
            this.listView.Name = "listView";
            this.listView.Size = new System.Drawing.Size(675, 447);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.ItemActivate += new System.EventHandler(this.ListView_ItemActivate);
            // 
            // indexColumn
            // 
            this.indexColumn.Text = "Индекс заказа";
            this.indexColumn.Width = 100;
            // 
            // processedColumn
            // 
            this.processedColumn.Text = "Обработан";
            this.processedColumn.Width = 100;
            // 
            // paidColumn
            // 
            this.paidColumn.Text = "Оплачен";
            this.paidColumn.Width = 100;
            // 
            // shippedColumn
            // 
            this.shippedColumn.Text = "Отгружен";
            this.shippedColumn.Width = 100;
            // 
            // executedColumn
            // 
            this.executedColumn.Text = "Исполнен";
            this.executedColumn.Width = 100;
            // 
            // OrdersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 450);
            this.Controls.Add(this.listView);
            this.Name = "OrdersForm";
            this.Text = "Список ваших заказов (кликните для оплаты)";
            this.Load += new System.EventHandler(this.OrdersForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader indexColumn;
        private System.Windows.Forms.ColumnHeader processedColumn;
        private System.Windows.Forms.ColumnHeader paidColumn;
        private System.Windows.Forms.ColumnHeader shippedColumn;
        private System.Windows.Forms.ColumnHeader executedColumn;
    }
}