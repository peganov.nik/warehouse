﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KekichWareHouse
{
    public partial class StatusEditForm : Form
    {
        public StatusEditForm()
        {
            InitializeComponent();
        }

        private void StatusEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (var item in orderStatusCheckedListBox.CheckedItems) {
                switch (item.ToString())
                {
                    case "Обработан":
                        OrdersForm.currentStatus |= (int)OrderStatus.Processed;
                        break;
                    case "Отгружен":
                        OrdersForm.currentStatus |= (int)OrderStatus.Shipped;
                        break;
                    case "Исполнен":
                        OrdersForm.currentStatus |= (int)OrderStatus.Executed;
                        break;
                }
            }
        }
    }
}
