﻿
namespace KekichWareHouse
{
    partial class StatusEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.orderStatusCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // orderStatusCheckedListBox
            // 
            this.orderStatusCheckedListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.orderStatusCheckedListBox.FormattingEnabled = true;
            this.orderStatusCheckedListBox.Items.AddRange(new object[] {
            "Обработан",
            "Отгружен",
            "Исполнен"});
            this.orderStatusCheckedListBox.Location = new System.Drawing.Point(0, -1);
            this.orderStatusCheckedListBox.Name = "orderStatusCheckedListBox";
            this.orderStatusCheckedListBox.Size = new System.Drawing.Size(188, 97);
            this.orderStatusCheckedListBox.TabIndex = 0;
            // 
            // StatusEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(187, 95);
            this.Controls.Add(this.orderStatusCheckedListBox);
            this.Name = "StatusEditForm";
            this.Text = "StatusEditForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StatusEditForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox orderStatusCheckedListBox;
    }
}