﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Форма, в которой пользователь может указать параметры автоматической генерации.
    /// </summary>
    public partial class GeneratorForm : Form
    {
        public GeneratorForm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Нажатие кнопки Ок.
        /// Проверка введённых данных.
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            // Проверка введённой информации.
            if (!uint.TryParse(chapterTextBox.Text, out uint chaptersCount) || chaptersCount == 0)
            {
                MessageBox.Show("Количество разделов должно быть положительным числом!",
                    "Ошибка");
            }
            else if (chaptersCount > 1073741823)
            {
                // Иначе при преобразовании из uint в int возникнет отрицательное число.
                MessageBox.Show("Количество разделов должно быть меньше 1073741823.",
                    "Ошибка");
            }
            else if (!uint.TryParse(productTextBox.Text, out uint productsCount))
            {
                MessageBox.Show("Количество товаров должно быть неотрицательным числом!",
                    "Ошибка");
            }
            else
            {
                Random randomGenerator = new Random();
                MainForm.baseChapters = Chapter.Generate(ref chaptersCount, ref productsCount, randomGenerator, true);
                var main = Owner as MainForm;
                main.DrawTreeView();
                Close();
            }
        }
        /// <summary>
        /// Нажатие кнопки Отмена. 
        /// Закрытие окна.
        /// </summary>
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
