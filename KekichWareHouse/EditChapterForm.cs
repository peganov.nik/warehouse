﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Форма, позволяющая редактировать и добавлять разделы.
    /// </summary>
    public partial class EditChapterForm : Form
    {
        /// <summary>
        /// Тип формы.
        /// </summary>
        private ChapterFormType type;
        /// <summary>
        /// Перечисление, в котором хранятся все возможные типы формы.
        /// </summary>
        public enum ChapterFormType
        {
            /// <summary>
            /// Добавление раздела.
            /// </summary>
            Add,
            /// <summary>
            /// Добавление раздела в корень.
            /// </summary>
            AddToRoot,
            /// <summary>
            /// Редактирование раздела.
            /// </summary>
            Edit
        }
        /// <summary>
        /// Создание формы переданного типа.
        /// </summary>
        /// <param name="type">Тип формы.</param>
        public EditChapterForm(ChapterFormType type)
        {
            this.type = type;
            InitializeComponent();
        }
        /// <summary>
        /// Загрузка формы и изменение текста в компонентах.
        /// </summary>
        private void EditChapterForm_Load(object sender, EventArgs e)
        {
            if (type == ChapterFormType.Edit)
            {
                Text = "Редактирование раздела";
                label.Text = "Введите новое название раздела:";
                okButton.Text = "Изменить";
                MainForm main = this.Owner as MainForm;
                // Указание название раздела в TextBox.
                nameTextBox.Text = main.treeView.SelectedNode.Text;
            }
            else
            {
                Text = "Добавление раздела";
                label.Text = "Введите название раздела:";
                okButton.Text = "Добавить";
            }
        }
        /// <summary>
        /// Проверяет, нет ли среди разделов такого же.
        /// </summary>
        /// <param name="currentNodes">Все разделы, в которых производится поиск.</param>
        /// <param name="newText">Имя нового раздела.</param>
        /// <returns>Есть ли такие же разделы.</returns>
        private bool CheckSameNodes(TreeNodeCollection currentNodes, string newText)
        {
            foreach (TreeNode node in currentNodes)
            {
                if (node.Text == newText)
                {
                    MessageBox.Show("В классификаторе не может быть двух одноименных разделов, имеющих общего родителя!",
                        "Ошибка");
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Нажатие кнопки ОК на форме. Добавление или редактирование раздела.
        /// </summary>
        private void OkButton_Click(object sender, EventArgs e)
        {
            MainForm main = this.Owner as MainForm;
            main.treeView.BeginUpdate();
            try
            {
                AddOrEditNode(main);
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Раздел не выбран. Попробуйте ещё раз.", "Ошибка");
            }
            catch (KeyNotFoundException)
            {
                MessageBox.Show("Выделенный раздел не найден в системе. Попробуйте ещё раз.", "Ошибка");
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("Программа не предназначена для параллельного запуска. Закройте другое окно.", "Ошибка");
            }
            catch
            {
                MessageBox.Show("Необработанное исключение.", "Ошибка");
            }
            main.treeView.EndUpdate();
            main.treeView.SelectedNode = null;
            this.Close();
        }
        /// <summary>
        /// Добавление или редактирование текущего раздела в зависимости от типа формы.
        /// </summary>
        /// <param name="main">Основная форма, из которой вызвана текущая.</param>
        private void AddOrEditNode(MainForm main)
        {
            if (main.treeView.SelectedNode == null || type == ChapterFormType.AddToRoot)
            {
                // Добавление в корень.
                AddNodeToRoot(main);
            }
            else
            {
                // Добавление в подразделы.
                AddSubNode(main);
            }
        }
        /// <summary>
        /// Добавление в подразделы.
        /// </summary>
        /// <param name="main">Основная форма, из которой вызвана текущая.</param>
        private void AddSubNode(MainForm main)
        {
            if (CheckSameNodes(main.treeView.SelectedNode.Nodes, nameTextBox.Text))
            {
                // Среди подразделов уже есть такой.
                main.treeView.EndUpdate();
                return;
            }
            if (type == ChapterFormType.Add)
            {
                // Добавление нового раздела.
                var newChapter = new Chapter(nameTextBox.Text);
                MainForm.findChapter[main.treeView.SelectedNode].sons.Add(newChapter);
                main.treeView.SelectedNode.Nodes.Add(nameTextBox.Text);
                MainForm.findChapter[main.treeView.SelectedNode.Nodes[main.treeView.SelectedNode.Nodes.Count - 1]] = newChapter;
            }
            else
            {
                // Редактирование.
                main.treeView.SelectedNode.Text = nameTextBox.Text;
                MainForm.findChapter[main.treeView.SelectedNode].name = nameTextBox.Text;
            }
        }
        /// <summary>
        /// Добавление нового раздела в корень.
        /// </summary>
        /// <param name="main">Основная форма, из которой вызвана текущая.</param>
        private void AddNodeToRoot(MainForm main)
        {
            if (CheckSameNodes(main.treeView.Nodes, nameTextBox.Text))
            {
                // Такие разделы уже есть.
                main.treeView.EndUpdate();
                return;
            }
            // Добавление нового раздела.
            main.treeView.Nodes.Add(nameTextBox.Text);
            var newChapter = new Chapter(nameTextBox.Text);
            MainForm.baseChapters.Add(newChapter);
            MainForm.findChapter[main.treeView.Nodes[main.treeView.Nodes.Count - 1]] = newChapter;
        }
        /// <summary>
        /// Нажатие кнопки "назад".
        /// </summary>
        private void BackButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
