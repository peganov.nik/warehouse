﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KekichWareHouse
{
    /// <summary>
    /// Класс для хранения информации о пользователе.
    /// </summary>
    [DataContract]
    public class User
    {
        /// <summary>
        /// ФИО пользователя.
        /// </summary>
        [DataMember]
        public string name;
        /// <summary>
        /// Телефон пользователя (В российском формате).
        /// </summary>
        [DataMember]
        public long phone;
        /// <summary>
        /// Адрес пользователя.
        /// </summary>
        [DataMember]
        public string adress;
        /// <summary>
        /// E-mail пользователя.
        /// </summary>
        [DataMember]
        public string email;
        /// <summary>
        /// Пароль пользователя.
        /// </summary>
        [DataMember]
        public string password;
        /// <summary>
        /// Текущая корзина пользователя.
        /// </summary>
        [DataMember]
        public List<Product> basket;
        /// <summary>
        /// Список всех зарегистрированных пользователей.
        /// </summary>
        public static User[] users;
        /// <summary>
        /// Конструктор, создающий нового пользователя из строк.
        /// В методе проверяются ошибки формата.
        /// </summary>
        /// <param name="name">ФИО пользователя.</param>
        /// <param name="phone">Телефон пользователя (В российском формате).</param>
        /// <param name="adress">Адрес пользователя.</param>
        /// <param name="email">E-mail пользователя.</param>
        /// <param name="password">Пароль пользователя.</param>
        public User(string name, string phone, string adress, string email, string password)
        {
            if (name == "")
            {
                throw new ArgumentException("ФИО не указано!");
            }
            if (!long.TryParse(phone, out this.phone))
            {
                throw new ArgumentException("Телефон в неверном формате!");
            }
            if (this.phone < 10000000000 || this.phone > 99999999999)
            {
                throw new ArgumentException("Телефон в неверном формате!");
            }
            if (adress == "")
            {
                throw new ArgumentException("Адрес не указан!");
            }
            if (email == "")
            {
                throw new ArgumentException("E-mail не указан!");
            }
            if (password == "")
            {
                throw new ArgumentException("Пароль не указан!");
            }
            if (Array.Exists(User.users, user => user?.email == email))
            {
                throw new ArgumentException("Пользователь с таким email уже есть в системе.");
            }
            try
            {
                // Используем встроенный класс для хранения e-mail.
                var eMailValidator = new System.Net.Mail.MailAddress(email);
            }
            catch (FormatException)
            {
                MessageBox.Show("Введите действительный адрес электронной почты!", "Ошибка!");
            }
            // Присваиваем значениям аргументы. 
            this.name = name;
            this.adress = adress;
            this.email = email;
            this.password = password;
            basket = new List<Product>();
        }
    }
}
